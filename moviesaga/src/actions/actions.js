import {
  GET_MOVIE_REQUEST,
  GET_MOVIE_ALL_REQUEST,
  GET_MOVIE_BY_ID_REQUEST, GET_ONE_MOVIE_REQUEST, GET_RECOMMENDATIONS_REQUEST, GET_CAST_REQUEST,
} from './actionTypes';

// getting Movies

export const getMovie = ({ payload }) => ({

  type: GET_MOVIE_REQUEST,
  payload: payload

});

//getting movie by id
export const getMovieId = ({ payload }) => ({
  type: GET_MOVIE_BY_ID_REQUEST,
  payload: payload
})

//getting one movie
export const getOneMovie=({payload})=>({
  type:GET_ONE_MOVIE_REQUEST,
  payload:payload
})

// recommendations

export const getRecommendations=({payload})=>({
  type:GET_RECOMMENDATIONS_REQUEST,
  payload:payload
})

//cast
export const getCast=({payload})=>({
  type:GET_CAST_REQUEST,
  payload:payload
})
