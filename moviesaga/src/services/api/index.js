import axios from 'axios';

import config from '../../config';

const request =axios.create({
  baseURL:config.API_ROOT
})

request.defaults.params={};
request.defaults.params['_f']='json';
request.defaults.params['api_key']=config.API_KEY;
request.defaults.headers.common['Accept']='application/json';
request.defaults.headers.common['Content-Type']='application.json; charset=utf-8';

request.interceptors.response.use(
  (response)=>response,
  (error)=>{
    return Promise.reject(error);
  }
);
export default {
  request,
}

