import React from 'react'
import config from "../config";
import get from "lodash/get";


const SingleCardComponent = ({movieId,loadingId}) => {

  console.log(movieId)

  const genres = get(movieId, "genres", []);

  console.log("Loading", genres)

  return (
        <div className="card-container">
          {movieId && !loadingId ?
            <div className="movie-id-info">
              <div className="movie-id-photo">
                <img src={`${config.API_IMAGE.medium}/${movieId.poster_path}`} alt="img" />
              </div>
              <div className="movie-id-text">
                <div className="movie-title">
                 <p>Title</p>
                 <h5>{movieId.title}</h5>
                </div>
                <div className="movie-overview">
                 <p>Overview</p>
                 <h5>{movieId.overview}</h5>
                  <p>Budget</p>
                  <h5>{movieId.budget}</h5>
                </div>
               <div className="genres-movie">
                {genres.length > 0 && genres.map((item) => (
                  <div className="genres-movie-item" key={item.id}>{item.name} </div>
                ))}
               </div>
              </div>
            </div>
            :<div className='loader'>Loading</div>
          }
        </div>

    )
}

export default SingleCardComponent
