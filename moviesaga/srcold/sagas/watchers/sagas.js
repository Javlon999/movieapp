import {call,put,takeLatest} from "redux-saga/effects";
import * as types from '../../actions/actionTypes';
import {api} from '../../services';
import config from "../../config";

function* fetchMovie({payload}){
  try {

    const {data}=yield call(api.request.get ,`/movie/now_playing?api_key=${config.API_KEY}&language=en-US&page=1`)
    // const {data} = yield call(api.request.get, `/movie/now_playing?page=${payload}&per-page=3&sort=-id&_f=json&api_key=${config.API_KEY}`);
    yield put({
      type: types.GET_MOVIE_SUCCESS,
      payload: data
    })

  }

  catch (error) {
    yield put({
      type: types.GET_MOVIE_FAIL,
      payload: error.response.data.message
    });
  }

}



export default function* saga(){
  yield takeLatest(types.GET_MOVIE_REQUEST,fetchMovie)
}
