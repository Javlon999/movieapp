import React from 'react'
import config from "../config";
import get from "lodash/get";


const UpComing = ({upcoming,loading}) => {

  console.log(upcoming)

  const upcomingMovie = get(upcoming[5], "poster_path", []);
  const upcomingMovieTitle = get(upcoming[5], "title", []);
  const upcomingMovieOverview = get(upcoming[5], "overview", []);
  const date=get(upcoming[3],'release_date',[]);

  return (
    <div style={{color:'#ffff'}} className="card-container">
      <h1 style={{color:'#ffff'}}>Movie</h1>
      {upcoming && !loading ?
        <div className="movie-id-info">

          <div className="movie-id-photo">
            <img src={`${config.API_IMAGE.medium}/${upcomingMovie}`} alt="img" />
          </div>
          <div className="movie-id-text">
            <div className="movie-title">
              <p>Title</p>
              <h5>{upcomingMovieTitle}</h5>
            </div>
            <div className="movie-overview">
              <p>Overview</p>
              <h5>{upcomingMovieOverview}</h5>
            </div>
            <div className="movie-date">
              <p>release_date</p>
              <h5>{date}</h5>
            </div>
          </div>
        </div>
        :<div className='loader'>Loading</div>
      }
    </div>

  )
}

export default UpComing
