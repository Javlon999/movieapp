import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";

import CastComponent from '../../components/CastComponent'
import SingleCardComponent from '../../components/SingleCardComponent';
import Recommendations from '../../components/Recommendations';
import { useHistory } from 'react-router-dom';
import '../../assets/styles/about.scss'
import { getMovieId ,getRecommendations,getCast} from '../../actions/index';

const SingleMoviePage = ({match}) => {
  const history = useHistory();
    const dispatch = useDispatch();
    const {id}=match.params
    const { loadingId, movieId,loadingRecommendations,getRecommendation} = useSelector(({ movie }) => ({
    loadingId: movie.loadingMovieId,
    movieId: movie.getMovieId,
    loadingRecommendations:movie.loadingRecommendations,
    getRecommendation:movie.getRecommendations

  }));
  const { loadingCast,getCastMovie} = useSelector(({ cast }) => ({
    loadingCast: cast.loadingCast,
    getCastMovie: cast.getCast,

  }));
    useEffect(()=>{
      dispatch(getMovieId({
        payload:id}))
      dispatch(getRecommendations({
        payload:{
          id:id,
          page:1
        }
      }))
      dispatch(getCast({
        payload:id
      }))
    },[])
  const toPush = (id) => {
    history.push(`/movies/${id}`)
  };
    return (
        <div className="all-movies-page">
            <div className="bod-container">
                <div className="body-section">
                    <SingleCardComponent movieId={movieId} loadingId={loadingId} />
                    <CastComponent loadingCast={loadingCast} getCastMovie={getCastMovie} />
                    <Recommendations recommendedMovie={getRecommendation} loading={loadingRecommendations} toPush={toPush}/>
                </div>
            </div>
        </div>
    )
}
export default SingleMoviePage;
