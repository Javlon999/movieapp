import React, { useEffect, useState } from 'react';
import { Route, Switch } from "react-router";
import './App.css';
// import CardComponent from './components/CardComponent';

import Movie from './containers/MoviesPage/Movie'
import SingleCardComponent from './components/SingleCardComponent'

import SingleMovie from './containers/SingleMovie/SingleMovie';

import Header from './components/Header'
import AboutMoviePage from "./containers/AboutMovie/AboutMovie";
function App() {



  return (
    <div className="App">

      <Switch>
        <Route exact path='/' component={Movie} />
        <Route exact path='/movies/:id' component={SingleMovie} />

      </Switch>
    </div>
  );
}

export default App;
