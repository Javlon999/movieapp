import {
  GET_MOVIE_REQUEST,
  GET_MOVIE_ALL_REQUEST,

} from './actionTypes';

// getting Movies

export const getMovie=({payload})=>({

    type: GET_MOVIE_REQUEST,
    payload: payload

});

export const getMovieAll=({payload})=>({
  type:GET_MOVIE_ALL_REQUEST,
  payload:payload
})
