import * as types from '../actions/actionTypes'

const initialState={
  loadingCast:false,
  getCast:{}
}



export default (state = initialState, action) => {

  switch(action.type){
    case types.GET_CAST_REQUEST:
      return {
        ...state,
        loadingCast:true,

      };
    case types.GET_CAST_SUCCESS:
      return {
        ...state,
        getCast:action.payload,
        loadingCast:false
      };
    case types.GET_CAST_FAIL:
      return {
        ...state,
        loadingCast:false
      };
    default :
      return {
        ...state
      };
  }

}

