import React from 'react'
import config from "../config";
import get from "lodash/get";

import '../assets/styles/allmovies.scss';
import {cutString} from "../services";


const CastComponent = ({ loadingCast,getCastMovie }) => {
console.log('Cast',getCastMovie)

  const Cast=get(getCastMovie,"cast",[])
  console.log("Cast",Cast);
  return (
    <div className="CastingName">
      <h1>Casting</h1>

    <div className="grid-container">

      {Cast ? Cast.map((item,key) => (
        <div key={item.id} className="grid-item">

          <div className="img-section">
            <img src={`${config.API_IMAGE.small}/${item.profile_path}`}  alt="img" />
            <p>{item.original_name}</p>
          </div>

        </div>
      )) : <div className='loader'>Loading</div>}

    </div>
    </div>

  )
}

export default CastComponent
