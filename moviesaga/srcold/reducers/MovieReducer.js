import * as types from '../actions/actionTypes'

const initialState = {
  loadingMovie: false,
  getMovie: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.GET_MOVIE_REQUEST:
      return {
        ...state,
        loadingMovie: true
      };

    case types.GET_MOVIE_SUCCESS:
      return {
        ...state,
        getMovie: action.payload,
        loadingMovie: false
      };

    case types.GET_MOVIE_FAIL:
      return {
        ...state,
        loadingMovie: false
      };

    default:
      return {...state};
  }
};



