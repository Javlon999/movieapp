import { all, fork } from "redux-saga/effects";



import saga from "./watchers/sagas";

export default function* rootSaga() {
  yield all([

    fork(saga),
  ]);
}
