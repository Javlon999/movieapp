import { call, put, takeLatest } from "redux-saga/effects";
import * as types from '../../actions/actionTypes';
import { api } from '../../services';
import config from "../../config";

function* fetchMovie({ payload }) {

  try {
    const { data } = yield call(api.request.get, `/movie/now_playing?api_key=${config.API_KEY}&language=en-US&page=${payload}`)

    yield put({
      type: types.GET_MOVIE_SUCCESS,
      payload: data
    })

  }

  catch (error) {
    yield put({
      type: types.GET_MOVIE_FAIL,
      payload: error.response.data.message
    });
  }
}

function* fetchMovieById({ payload }) {
  try {
    const { data } = yield call(api.request.get, `/movie/${payload}?api_key=${config.API_KEY}&language=en-US`)
    yield put({
      type: types.GET_MOVIE_BY_ID_SUCCESS,
      payload: data
    })
  }
  catch (error) {
    yield put({
      type: types.GET_MOVIE_BY_ID_FAIL
    })
  }
}

function* fetchOneMovie({payload}){
  try {
    const {data}=yield call(api.request.get,`/movie/upcoming?api_key=${config.API_KEY}&language=en-US&page=${payload}`)

    yield put ({
      type:types.GET_ONE_MOVIE_SUCCESS,
      payload:data.results
    })
  }
  catch (error) {
    yield put({
      type:types.GET_ONE_MOVIE_FAIL
    })
  }
}
function* fetchRecommendations({payload}){
  try{

    const {data}=yield call(api.request.get,`/movie/${payload.id}/recommendations?api_key=${config.API_KEY}&language=en-US&page=${payload.page}`)

    yield put ({
      type:types.GET_RECOMMENDATIONS_SUCCESS,
      payload:data
    })
  }
  catch(error){
    yield put({
      type:types.GET_RECOMMENDATIONS_FAIL
    })
  }
}
function* fetchCast({payload}){
  try{
    const {data} = yield call(api.request.get, `/movie/${payload}/credits?&_f=json&api_key=${config.API_KEY}`);
    yield put ({
      type:types.GET_CAST_SUCCESS,
      payload:data
    })
  }
  catch(error){
    yield put ({
      type:types.GET_CAST_FAIL,
    })
  }
}

export default function* saga() {
  yield takeLatest(types.GET_MOVIE_REQUEST, fetchMovie)
  yield takeLatest(types.GET_MOVIE_BY_ID_REQUEST, fetchMovieById)
  yield takeLatest(types.GET_ONE_MOVIE_REQUEST,fetchOneMovie)
  yield takeLatest(types.GET_RECOMMENDATIONS_REQUEST,fetchRecommendations)
  yield takeLatest(types.GET_CAST_REQUEST,fetchCast)
}
