import React from 'react'
import config from "../config";

import { cutString } from "../services";
import get from "lodash/get";
import '../assets/styles/allmovies.scss';

const CardComponent = ({ movie, loading, toPush }) => {

    return (
        <div className="grid-container">
            {console.log(movie)}
            {movie ? movie.map((item, key) => (
                <div key={item.id} className="grid-item">
                    <div className="img-section">
                        <img src={`${config.API_IMAGE.small}/${item.poster_path}`} alt="img" />
                    </div>
                    <div className="text-section">
                        <div className="movie-name">
                            <span>{item.original_title && cutString(item.original_title, 15)}...</span>
                        </div>
                        <div className="movie-star">
                            <span>☆</span><span>{item.vote_average}</span>
                        </div>
                    </div>
                  <div className="button-section">
                        <button className="toPush"
                            onClick={() => toPush(item.id)}
                        >VIEW</button>
                    </div>

                </div>
            )) : <div className='loader'>Loading</div>}

        </div>

    )
}

export default CardComponent
