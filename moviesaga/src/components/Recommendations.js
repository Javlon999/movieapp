import React from 'react'
import config from "../config";
import get from "lodash/get";
import {cutString} from "../services";

import '../assets/styles/allmovies.scss'
const Recommendations = ({recommendedMovie,loading,toPush}) => {

  console.log(recommendedMovie)

  const results = get(recommendedMovie, "results", []);
console.log('results',results)
  console.log("Loading", loading)
  return (
    <div className="Maingrid-container">
      <h1>Recommended Movies</h1>
      {results ?

        <div className="grid-container">

              {results.length > 0 && results.map((item) => (
                <div key={item.id} className="grid-item">

                  <div className="img-section">

                  <img src={`${config.API_IMAGE.small}/${item.poster_path}`} alt="img" />
                    <div className="text-section">
                      <div className="movie-name">
                        <span>{item.original_title && cutString(item.original_title, 15)}...</span>
                      </div>
                      <div className="movie-star">
                        <span>☆</span><span>{item.vote_average}</span>
                      </div>
                    </div>
                    <div className="button-section">
                      <button className="toPush"
                              onClick={() => toPush(item.id)}
                      >VIEW</button>
                    </div>
                </div>
                </div>
              ))}
          </div>

        :<div className='loader'>Loading</div>}
</div>

  )
}
export default Recommendations
