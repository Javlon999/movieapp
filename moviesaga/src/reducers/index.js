import { combineReducers } from "redux";
import MovieReducer from "./MovieReducer";
import CastReducer from "./CastReducer";

const reducers = combineReducers({
   movie: MovieReducer,
   cast:CastReducer,

});

export default reducers;
