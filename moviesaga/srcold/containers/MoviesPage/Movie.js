import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {useHistory} from 'react-router-dom';

import config from '../../config'

import {getMovie} from '../../actions/index';

const MoviePage = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [page, setPage] = useState(1);

  const { loading, allMovie, } = useSelector(({movie })=> ({
    loading: movie.loadingMovie,
    allMovie: movie.getMovie.results,
  }));


useEffect(() => {
    dispatch(getMovie({
      payload: 2
    }));

  }, []);



  console.log(allMovie)

  return (
    <div className="all-movies-page">

          <div className="bod-container">
        <div className="body-section">
          <h1>Movie Lists</h1>

        </div>
      </div>
    </div>
  )


}
export default MoviePage;
