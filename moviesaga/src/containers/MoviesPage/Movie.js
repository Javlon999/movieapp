import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from 'react-router-dom';
import CardComponent from '../../components/CardComponent';
import UpComing from '../../components/UpComing';
import config from '../../config'

import { getMovie,getOneMovie } from '../../actions/index';

const MoviePage = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [page, setPage] = useState(1);

  const { loading, movie, } = useSelector(({ movie }) => ({
    loading: movie.loadingMovie,
    movie: movie.getMovie,
  }));

 const {loadingUpComing,upComingMovie}=useSelector(({movie})=>({
    loadingUpComing:movie.loadingOneMovie,
    upComingMovie:movie.getOneMovie
  }));

  useEffect(() => {
    dispatch(getOneMovie({
      payload:5
    }))
    dispatch(getMovie({
      payload: page
    }));
    window.scrollTo(0, 0)

  }, []);

  const handlerPage = () => {
    setPage(page + 1);

    if (movie.total_pages > page) {
      dispatch(getMovie({
        payload: page + 1
      }));
    } else {
      setPage(1);
      dispatch(getMovie({
        payload: 1
      }));
    }
  };
  const toPush = (id) => {
    history.push(`/movies/${id}`)
  };

  return (
    <div className="all-movies-page">
      <div className="bod-container">
        <div className="body-section">
          <UpComing upcoming={upComingMovie} loading={loadingUpComing}/>
          <CardComponent movie={movie.results} loading={loading} toPush={toPush} />
        </div>
        <div className="body-button-section">
          <button onClick={handlerPage}>LOAD MORE</button>
        </div>
      </div>
    </div>
  )


}
export default MoviePage;
