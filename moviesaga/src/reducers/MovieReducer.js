import * as types from '../actions/actionTypes'

const initialState = {
  loadingMovie: false,
  getMovie: {},

  loadingMovieId: false,
  getMovieId: {},

  loadingOneMovie:false,
  getOneMovie:{},

  loadingRecommendations:false,
  getRecommendations:{},


};

export default (state = initialState, action) => {
  switch (action.type) {
    case types.GET_MOVIE_REQUEST:
      return {
        ...state,
        loadingMovie: true
      };

    case types.GET_MOVIE_SUCCESS:
      return {
        ...state,
        getMovie: action.payload,
        loadingMovie: false
      };

    case types.GET_MOVIE_FAIL:
      return {
        ...state,
        loadingMovie: false
      };
    case types.GET_MOVIE_BY_ID_REQUEST:
      return {
        ...state,
        loadingMovieId: true
      };
    case types.GET_MOVIE_BY_ID_SUCCESS:
      return {
        ...state,
        getMovieId: action.payload,
        loadingMovieId: false
      };
    case types.GET_MOVIE_BY_ID_FAIL:
      return {
        ...state,
        loadingMovie: false
      };
    case types.GET_ONE_MOVIE_REQUEST:
      return {
        ...state,
        loadingOneMovie: true
      }
    case types.GET_ONE_MOVIE_SUCCESS:
      return {
        ...state,
        getOneMovie: action.payload,
        loadingOneMovie:false
      }
    case types.GET_ONE_MOVIE_FAIL:
      return {
        ...state,
        loadingOneMovie:false
      }
    case types.GET_RECOMMENDATIONS_REQUEST:
      return {
        ...state,
        loadingRecommendations:true

      }
    case types.GET_RECOMMENDATIONS_SUCCESS:
      return {
        ...state,
        getRecommendations: action.payload,
        loadingRecommendations:false

      }
    case types.GET_RECOMMENDATIONS_FAIL:
      return {
        ...state,
        loadingRecommendations:false
      }

    default:
      return { ...state };
  }
};



